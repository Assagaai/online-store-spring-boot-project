package com.oneline_store.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FinishPurchaseResponse {

    private Integer orderId;
}
