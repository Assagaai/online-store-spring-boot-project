package com.oneline_store.controller.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.util.Map;

@Data
@ToString(exclude = "password")
public class FinishPurchaseRequest {

    @NotNull
    private Map<Integer,Integer>ProductIdProductCount;
    @NotEmpty
    private String username;
    private String userSurname;
    @NotEmpty
    private String email;
    @NotEmpty
    private String phone;
    @NotEmpty
    private String address;
    private String comment;
    private String password;
}
