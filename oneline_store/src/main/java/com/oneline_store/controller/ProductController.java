package com.oneline_store.controller;

import com.oneline_store.persistence.entity.Products;
import com.oneline_store.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/api/auth")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/products")
    public List<Products> getAllProducts() {
        log.info("handling get all products request");
        return productService.findAll();
    }

    @GetMapping("/product/{id}")
    public Products findById(@PathVariable Integer id) {
        log.info("handling get product by id: {}", id);
        return productService.findById(id);
    }
}
