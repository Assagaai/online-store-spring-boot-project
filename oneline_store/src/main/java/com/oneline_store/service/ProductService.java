package com.oneline_store.service;

import com.oneline_store.persistence.ProductsRepository;
import com.oneline_store.persistence.entity.Products;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductsRepository productsRepository;

    public List<Products> findAll(){
        return productsRepository.findAll();
    }

    @SneakyThrows
    public Products findById(Integer id){
        return productsRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("No product found"));
    }
}
