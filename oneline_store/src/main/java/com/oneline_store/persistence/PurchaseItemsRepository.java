package com.oneline_store.persistence;

import com.oneline_store.persistence.entity.PurchaseItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseItemsRepository extends JpaRepository<PurchaseItems, Integer> {
}
