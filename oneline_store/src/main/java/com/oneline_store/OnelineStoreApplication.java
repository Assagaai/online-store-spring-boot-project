package com.oneline_store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnelineStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnelineStoreApplication.class, args);
	}

}
