package com.oneline_store.persistence;

import com.oneline_store.persistence.entity.ERole;
import com.oneline_store.persistence.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    Optional<Role> findByName(ERole name);
}
