package com.oneline_store.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_table")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    Users users;

    @Column(name = "comments")
    private String comments;


}