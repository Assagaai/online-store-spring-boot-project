DROP TABLE IF EXISTS product_table;
create table product_table(
                              product_id int primary key auto_increment,
                              title varchar(50) not null,
                              description text,
                              image text,
                              price double
);

Drop table if exists user_table;
create table user_table(
                           user_id int primary key auto_increment,
                           first_name varchar(50) not null,
                           last_name varchar(50),
                           email varchar(50) not null,
                           phone varchar(15),
                           password text,
                           role text,
                           address varchar(70)
);

create unique index user_email on user_table(email);

Drop table if exists purchase_item;
create table purchase_item(
                              purchase_id int primary key auto_increment,
                              product_id int not null constraint product_id references product_table(product_id),
                              count int default (1),
                              order_id int not null constraint order_id references order_table(order_id)

);

Drop table if exists order_table;
create table order_table(
                            order_id int primary key auto_increment,
                            user_id int not null constraint user_id references user_table(user_id),
                            comments text
);