package com.oneline_store.persistence.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "purchase_item")
public class PurchaseItems {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PURCHASE_ITEM_ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    Products products;

    @Column(name = "count")
    private Integer count;

    @ManyToOne
    @JoinColumn(name = "ORDER_ID")
    Orders orders;
}