package com.oneline_store.service;

import com.oneline_store.controller.dto.FinishPurchaseRequest;
import com.oneline_store.persistence.OrdersRepository;
import com.oneline_store.persistence.entity.Orders;
import com.oneline_store.persistence.entity.Products;
import com.oneline_store.persistence.entity.PurchaseItems;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class PurchaseService {

    final private ProductService productService;
    final private UserService userService;
    final private OrdersRepository ordersRepository;


    public Integer finishPurchase(FinishPurchaseRequest request){
        log.info("creating order entity from request: {}", request);
        Orders order = new Orders();
        order.setUsers(userService.findOrCreateUser(request));
        order.setComments(request.getComment());
        order = ordersRepository.save(order);

        Orders finalOrder = order;
        request.getProductIdProductCount().forEach(
                (k,v) -> {
                    Products product = productService.findById(k);
                    PurchaseItems p = new PurchaseItems();
                    p.setProducts(product);
                    p.setCount(v);
                    p.setOrders(finalOrder);
                }
        );
        return finalOrder.getId();
    }
}
